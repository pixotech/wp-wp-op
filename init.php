<?php

require_once __DIR__ . '/src/Autoloader.php';

spl_autoload_register(new \WordPress\Pixo\Outpost\Autoloader('WordPress\\Pixo\\Outpost', __DIR__ . '/src/'));
