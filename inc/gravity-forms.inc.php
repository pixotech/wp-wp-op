<?php // gravity-forms.inc.php

function pixo_remove_gravity_forms_fields() {
	add_filter( 'gform_add_field_buttons', 'pixo_gf_remove_gravity_fields' );
}

function pixo_gf_remove_gravity_fields( $field_groups ) {

	foreach ($field_groups as $key => $group) {
		if($group['name'] == 'standard_fields') {
			pixo_gf_remove_some_fields($field_groups, $key, $group, 'standard_fields');
		} elseif($group['name'] == 'advanced_fields') {
			pixo_gf_remove_some_fields($field_groups, $key, $group, 'advanced_fields');
		} elseif ($group['name'] == 'post_fields') {
			unset($field_groups[$key]);
		} elseif ($group['name'] == 'pricing_fields') {
			unset($field_groups[$key]);
		}
	}

	return $field_groups;
}

function pixo_gf_remove_some_fields(&$field_groups, $key, $group, $fields_group_name) {
	$func = 'pixo_gf_get_' . $fields_group_name . '_to_remove';
	$fields_to_remove = call_user_func($func);
	foreach ($group['fields'] as $k => $field) {
		if(in_array($field['data-type'], $fields_to_remove))
			unset($field_groups[$key]['fields'][$k]);
	}
}

function pixo_gf_get_standard_fields_to_remove() {
	$default = [
		'multiselect',
		'page',
		'html',
	];
	return apply_filters('wp_op_gf_standard_fields_to_remove', $default);
}

function pixo_gf_get_advanced_fields_to_remove() {
	$default = [
		'captcha',
		'fileupload',
		'list',
	];
	return apply_filters('wp_op_gf_advanced_fields_to_remove', $default);
}