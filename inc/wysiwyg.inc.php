<?php

function pixo_wyiwyg_outpost_links($results, $query) {
  foreach($results as $k => $node) {
    $url_parts = parse_url($node['permalink']);
    $results[$k]['permalink'] = $url_parts['path'];
    if(isset($url_parts['query']))
      $results[$k]['permalink'] .= '?' . $url_parts['query'];
    if(isset($url_parts['fragment']))
      $results[$k]['permalink'] .= '#' . $url_parts['fragment'];
  }

  return $results;
}
