<?php // preview.inc.php

function pixo_template_redirect() {
  if( !isset($_GET['preview_id']) && !isset($_GET['page_id']) )
    return;
  if( !$id = pixo_preview_authorized() )
    return;
  $new_content = isset($_GET['page_id']) ? TRUE : FALSE;
  $post = pixo_get_preview_content($id, $new_content);
  pixo_shape_preview_data($post);
  $preview = pixo_wp_preview_curl($post);
  print $preview;
  die;
}

function pixo_preview_authorized() {
  if ( isset($_GET['preview_id']) && isset($_GET['preview_nonce']) ) {
    $id = (int) $_GET['preview_id'];

    if ( false == wp_verify_nonce( $_GET['preview_nonce'], 'post_preview_' . $id ) )
      wp_die( __('You do not have permission to preview drafts.') );
    return $id;
  } elseif( isset($_GET['preview']) && $_GET['preview'] == true && isset($_GET['page_id'])) {
    $id = (int) $_GET['page_id'];
    return $id;
  } else {
    return FALSE;
  }
}

function pixo_get_preview_content($id, $new_content) {
  $user = wp_get_current_user();
  if ($new_content) {
    $post = get_post($id);
  } else {
    $post = wp_get_post_autosave($id, $user->ID);
    $post = sanitize_post($post);
  }
  $post->post_content = wpautop($post->post_content);
  $post->post_excerpt = wpautop($post->post_excerpt);
  $post->content_type = get_post_type($id);
  $post->acf = get_fields($post->ID);
  return $post;
}

function pixo_shape_preview_data(&$post) {
  $obj_vars = get_object_vars($post);
  $exceptions = [
    'post_content_filtered',
    'post_name',
    'post_password',
    'post_mime_type',
    'comment_count'
  ];
  foreach ($obj_vars as $prop => $val) {
    if(!in_array($prop, $exceptions) && substr($prop, 0, 5) == 'post_') {
      $new_prop = substr($prop, 5);
      $post->$new_prop = $val;
      unset($post->$prop);
    }
  }
}

function pixo_wp_preview_curl($post) {
  $ch = wp_op_prep_curl_connection($post, '/_preview');
  if(!$output = curl_exec($ch)){
    $message = 'Error: "' . curl_error($ch) . '" - Code: ' . curl_errno($ch);
    die($message);
  }
  curl_close($ch);
  return $output;
}
