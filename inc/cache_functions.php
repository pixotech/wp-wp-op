<?php // cache_functions.php

function wp_op_save_post($post_id) {
  $id = $post_id;
  if($rev = wp_is_post_revision($post_id))
    $id = $rev;
  wp_op_cache_change(['post_update' => [
    'id' => $id, 
    'type' => get_post_type($id)
  ]]);
}

function wp_op_after_delete_post($post_id) {
  wp_op_cache_change(['post_delete' => $post_id]);
}

function wp_op_acf_get_options_page($page, $slug) {
  if(isset($_GET['message']))
    wp_op_cache_change(['options_update' => $slug]);
  return $page;
}

function wp_op_wp_update_nav_menu($id, $data = false) {
  wp_op_cache_change(['menu' => [
    'id' => $id, 
    'name' => $data['menu-name']
  ]]);
}

function wp_op_cache_change($data) {
  $ch = wp_op_prep_curl_connection($data, '/_wordpress_cache');
  $output = curl_exec($ch);
  wp_op_catch_curl_errors($ch);
  curl_close($ch);
}

function wp_op_catch_curl_errors($ch) {
  $info = curl_getinfo($ch);
  $WP_Error = new WP_Error;
  
  if(curl_errno($ch)){
    $message = '{WP-OP} Error: "' . curl_error($ch) . '" - Code: ' . curl_errno($ch) . '.  HTTP status code: ' . $info['http_code'];
    $WP_Error->add('op_cache_error', __( $message ));
  }
  if($info['http_code'] != '200')
    $WP_Error->add('Non-200_on_cache_busting', __("{WP-OP} Outpost returned $info[http_code] in attempt to bust cache."));

  if(is_wp_error($WP_Error)) {
    foreach ($WP_Error->get_error_messages() as $error)
      error_log($error);
  }
}
