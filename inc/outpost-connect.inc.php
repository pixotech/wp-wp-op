<?php 

use WordPress\Pixo\Outpost\WPOP;

function wp_op_prep_curl_connection($data, $route) {
  $base = pixo_get_frontend_url();
  $url = $base . $route;
  $ch = curl_init($url);
  $options = array(
    CURLOPT_RETURNTRANSFER => 1,
    CURLOPT_HEADER => 0,
    CURLOPT_HTTPHEADER => array('Accept: application/json', "Content-type: application/json"),
    CURLOPT_TIMEOUT => 10,
    CURLOPT_POSTFIELDS => json_encode($data)
  );
  if(strpos($base, WPOP::getQADomain()) !== FALSE)
    pixo_add_curl_basic_auth($options);
  curl_setopt_array($ch, $options);
  return $ch;
}

function pixo_add_curl_basic_auth(&$options) {
  $path = WPOP::getOutpostSecretsFile();
  if(file_exists($path)) {
    $opts = $options;
    $secrets = json_decode(file_get_contents($path), true);
    $auth = [
      CURLOPT_USERPWD => $secrets['wordpress']['http']['username'] . ':' . $secrets['wordpress']['http']['password'], 
      CURLOPT_HTTPAUTH => CURLAUTH_BASIC
    ];
    $options = $opts + $auth;
  }
}

function pixo_get_frontend_url() {
  $url = parse_url($_SERVER['HTTP_HOST']);
  $tld = pixo_get_tld($url['path']);
  $scheme = isset($_SERVER['REQUEST_SCHEME']) ? $_SERVER['REQUEST_SCHEME'] : 'http';
  return "$scheme://" . WPOP::getOutpostFrontendDomain() . '.' . $tld;
}

function pixo_get_tld($host) {
  $path = explode('.', $host);
  $k = array_search(WPOP::getOutpostFrontendDomain(), $path);
  return implode('.', array_slice($path, $k+1));
}
