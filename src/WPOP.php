<?php

namespace WordPress\Pixo\Outpost;
use Dotenv\Dotenv;

class WPOP {
    
    protected static $OutpostFrontendDomain;
    protected static $QADomain;
    protected static $SecretsAutoload;
    protected static $OutpostSecretsFile;

    public static function init()
    {
        self::setOutpostFrontendDomain();
        self::setQADomain();
        self::setSecretsAutoload();
        self::setOutpostSecretsFile();
        self::loadOutpostSecrets();
    }

    protected static function setOutpostFrontendDomain()
    {
        /**
         * Implementations of the `wp_op_frontend_domain` 
         * hook should return just the part without subdomains 
         * and without the TLD.  For example, if the full prod 
         * domain is http://www.example.com, this hook should 
         * return just "example".
         */
        self::$OutpostFrontendDomain = apply_filters('wp_op_frontend_domain', '');
    }

    protected static function setQADomain()
    {
        /**
         * Implementations of the `wp_op_qa_domain` hook should 
         * return a full domain, including any subdomains and 
         * TLD.  For example, "example.pixotech.com".
         */
        self::$QADomain = 'qa.project.com';
        self::$QADomain = apply_filters('wp_op_qa_domain', self::$QADomain);
    }

    protected static function setSecretsAutoload()
    {
        $default_autoload = ABSPATH . '../../vendor/autoload.php';
        self::$SecretsAutoload = apply_filters('wp_op_secrets_autoload', $default_autoload);
    }

    protected static function setOutpostSecretsFile()
    {
        
        $default_secrets_file = [
            'path' => ABSPATH . '../../',
            'file' => '.env'
        ];
        /**
         * Implementations of the `wp_op_secrets_file` 
         * hook should return an array with 'path' and 
         * 'file' keys.
         */
        self::$OutpostSecretsFile = apply_filters('wp_op_secrets_file', $default_secrets_file);
    }

    protected static function loadOutpostSecrets()
    {
        require_once self::$SecretsAutoload;
        if (is_file(self::$OutpostSecretsFile['path'] . self::$OutpostSecretsFile['file'])) {
            $dotenv = new Dotenv(self::$OutpostSecretsFile['path']);
            $dotenv->load();
        }
    }

    public static function getOutpostFrontendDomain()
    {
        return self::$OutpostFrontendDomain;
    }

    public static function getQADomain()
    {
        return self::$QADomain;
    }

    public static function getSecretsAutoload()
    {
        return self::$SecretsAutoload;
    }

    public static function getOutpostSecretsFile()
    {
        return self::$OutpostSecretsFile;
    }
}