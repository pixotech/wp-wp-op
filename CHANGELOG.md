# WordPress-Outpost Changelog

## 0.3.8

Bugfix: PHP requires semicolons.  Who knew?

## 0.3.7

- Bugfix: Missing '.' between domain and tld for outpost-connect.
- Bugfix: Set a default QADomain so we fail gracefully without an implementation of 'wp_op_qa_domain' hook.

## 0.3.6

Bugfix: WPOP::init() needs to be called after wp plugins have loaded.

## 0.3.5

PHP didn't like WPOP as a static class, and fix incorrect namepsace.

## 0.3.3 and 0.3.4

More copy pasta typos.  Accidentally tagged v0.3.2 as v0.3.3, so jumping to v0.3.4.

## 0.3.2

Init.php had incorrect dir name for Autoloader.

## 0.3.1

Clarify details about filter hooks in README.  

## 0.3.0

Use Dotenv for secrets.
