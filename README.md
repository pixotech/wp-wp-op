# Pixo WP-OP plugin

This plugin fires off to Outpost a signal that part of the Outpost cache should be rebuilt because content has changed.  
This plugin also enables WordPress previews to be themed by Outpost.  
NOTE: the options section has been disabled as it was firing way too often.  

## Setup
Implement the following hooks:

* `wp_op_frontend_domain`
* `wp_op_qa_domain` (optional -- used to know if need to add basic auth to curl call)
* `wp_op_secrets_file` (optional -- defaults to .env two dirs above wp docroot)

Each hook is documented in [src/WPOP.php](src/WPOP.php).

## Gravity Forms
Until Outpost has full Gravity Forms support, we have to remove
some GF fields from what the user can place into a form.  Defaults are defined in `pixo_gf_get_standard_fields_to_remove` and `pixo_gf_get_advanced_fields_to_remove`.  To change remove fields other than the defaults, implement either the `wp_op_gf_standard_fields_to_remove` or `wp_op_gf_advanced_fields_to_remove` filter hooks depending on which field type it is.  All these functions are in [inc/gravity-forms.inc.php](inc/gravity-forms.inc.php).

## Data sent to Outpost
Data sent to Outpost will look like the following:

### Post updated
```
['post_update' => $post_id]
```

### Post deleted
```
['post_delete' => $post_id]
```

### Options updated
This is like the home page settings form and the footer settings.

```
['options_update' => $slug]
```
Where $slug could be something like 'homepage-settings' or 'footer-settings'.  

### Menu changed
```
['menu' => ['id' => $id, 'name' => $data['menu-name']]]
```