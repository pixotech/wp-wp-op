<?php
/*
Plugin Name: WordPress-Outpost
Version: 0.3.8
Description: Connects WordPress and Outpost for busting parts of Outpost cache when content and menus are updated in WordPress, and for supporting previews being themed by Outpost.  
Author: Pixo &mdash; Charles Suggs
Author URI: http://www.pixotech.com
Text Domain: wp-op
Domain Path: /languages 
*/

require_once 'init.php';
require_once "inc/outpost-connect.inc.php";
require_once "inc/cache_functions.php";
require_once "inc/preview.inc.php";
require_once "inc/wysiwyg.inc.php";
require_once "inc/gravity-forms.inc.php";

add_action('plugins_loaded', ['\WordPress\Pixo\Outpost\WPOP', 'init']);

add_action('save_post', 'wp_op_save_post');
add_action('after_delete_post', 'wp_op_after_delete_post');

/**
 * This hook fires whenever an ACF options page is 
 * loaded. If it's loading post-save, then $_GET 
 * temporarily has a 'message' element that we check 
 * for. If it exists, we assume the page has been saved.
 */
// add_filter('acf/get_options_page', 'wp_op_acf_get_options_page', 10, 2);
add_action('wp_update_nav_menu', 'wp_op_wp_update_nav_menu', 10, 2);

// Outpost Preview Support
add_action( 'template_redirect', 'pixo_template_redirect' );

// Make sure permalinks in wysiwygs point to the public site.
add_filter( 'wp_link_query', 'pixo_wyiwyg_outpost_links', 10, 2 );

// Customize Gravity Forms
pixo_remove_gravity_forms_fields();
